package com.cg.curlygirl.model.dto;

import java.math.BigDecimal;
import java.util.Base64;
import java.util.List;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import com.cg.curlygirl.model.ConditionerType;
import com.cg.curlygirl.model.entity.Ingredient;

public class ConditionerDTO {
	
	@NotBlank
	private String name;
	@NotBlank
	private String brand;
	@PositiveOrZero
	@Digits(integer = 6, fraction= 2)
	private BigDecimal price;
	private String placeToBuy;
	@NotNull
	private ConditionerType type;
	@NotEmpty
	private List<Ingredient> ingredients;
	private Base64 image;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getPlaceToBuy() {
		return placeToBuy;
	}
	public void setPlaceToBuy(String placeToBuy) {
		this.placeToBuy = placeToBuy;
	}
	public ConditionerType getType() {
		return type;
	}
	public void setType(ConditionerType type) {
		this.type = type;
	}
	public List<Ingredient> getIngredients() {
		return ingredients;
	}
	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	public Base64 getImage() {
		return image;
	}
	public void setImage(Base64 image) {
		this.image = image;
	}	
}
