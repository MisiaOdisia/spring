package com.cg.curlygirl.controller;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cg.curlygirl.model.dto.ConditionerDTO;
import com.cg.curlygirl.model.entity.Conditioner;
import com.cg.curlygirl.repository.ConditionerRepository;

@RestController
@RequestMapping("api/v1/conditioners")
public class ConditionerController {
	
	@Autowired
	private ConditionerRepository conditionerRepository;	
	@Autowired
    private ModelMapper modelMapper;
	
	@GetMapping
	public Iterable<Conditioner> getAll() {
		return conditionerRepository.findAll();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Conditioner> get(@PathVariable("id") long id) {
		return conditionerRepository.findById(id)
				.map(ResponseEntity::ok)
				.orElse(new ResponseEntity<Conditioner>(HttpStatus.NOT_FOUND));
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public long createConditioner(@Valid @RequestBody ConditionerDTO conditionerDTO) {
		return conditionerRepository.save(convertToEntity(conditionerDTO)).getId();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Conditioner> updateConditioner(@PathVariable("id") long id, @RequestBody ConditionerDTO conditionerDTO) {
		return conditionerRepository.findById(id)
				.map(conditioner -> {					
					conditioner.setId(id);
					return new ResponseEntity<Conditioner>(conditionerRepository.save(conditioner), HttpStatus.OK);
				}).orElse(new ResponseEntity<Conditioner>(HttpStatus.NOT_FOUND));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<HttpStatus> deleteConditioner(@PathVariable("id") long id) {		
		return conditionerRepository.findById(id)
			.map(conditioner -> {
				conditionerRepository.deleteById(conditioner.getId());
				return new ResponseEntity<HttpStatus>(HttpStatus.OK);
			}).orElse(new ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND));		
	}
	
	private Conditioner convertToEntity(ConditionerDTO conditionerDTO) {
	    return  modelMapper.map(conditionerDTO, Conditioner.class);
	}
}
