package com.cg.curlygirl.model;

public enum ConditionerType {
	PROTEIN,
	HUMECTANT,
	EMOLLIENT
}
