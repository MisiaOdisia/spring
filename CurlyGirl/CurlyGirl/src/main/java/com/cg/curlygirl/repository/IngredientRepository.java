package com.cg.curlygirl.repository;

import org.springframework.data.repository.CrudRepository;

import com.cg.curlygirl.model.entity.Ingredient;

public interface IngredientRepository extends CrudRepository<Ingredient, Long>{

}
