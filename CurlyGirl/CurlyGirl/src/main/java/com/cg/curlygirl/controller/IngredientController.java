package com.cg.curlygirl.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cg.curlygirl.model.dto.IngredientDTO;
import com.cg.curlygirl.model.entity.Ingredient;
import com.cg.curlygirl.repository.IngredientRepository;

@RestController
@RequestMapping("api/v1/ingredients")
public class IngredientController {
	
	@Autowired
	private IngredientRepository ingredientRepository;
	@Autowired
    private ModelMapper modelMapper;
	
	@GetMapping
	public Iterable<Ingredient> getAll() {
		return ingredientRepository.findAll();
	}
	
	@GetMapping("/{id}")
	public Optional<Ingredient> get(@PathVariable("id") long id) {
		return ingredientRepository.findById(id);
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public long createIngredient(@Valid @RequestBody IngredientDTO ingredientDTO) {
		Ingredient ingredient = convertToEntity(ingredientDTO);
		return ingredientRepository.save(ingredient).getId();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Ingredient> updateIngredient(@PathVariable("id") long id, @RequestBody IngredientDTO ingredientDTO) {
		Ingredient ingredient = convertToEntity(ingredientDTO);
		
		if(ingredientRepository.findById(id).isPresent()) {
			ingredient.setId(id);
			return new ResponseEntity<>(ingredientRepository.save(ingredient), HttpStatus.OK);
		}
		else 
			return new ResponseEntity<>(ingredient, HttpStatus.BAD_REQUEST);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<HttpStatus> deleteIngredient(@PathVariable("id") long id) {
		if(ingredientRepository.findById(id).isPresent()) {
			ingredientRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.OK);
		}
		else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@PostMapping("/analyze")
	@ResponseStatus(HttpStatus.OK)
	public boolean analyze(@RequestBody String ingredients) {
		//TODO
		boolean isCompliesWithCg = true;
		return isCompliesWithCg; 
	}
	
	private Ingredient convertToEntity(IngredientDTO ingredientDTO) {
	    return modelMapper.map(ingredientDTO, Ingredient.class);
	}
}
