package com.cg.curlygirl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurlyGirlApplication {

	public static void main(String[] args) {
		SpringApplication.run(CurlyGirlApplication.class, args);
	}

}
