package com.cg.curlygirl.model.entity;

import java.math.BigDecimal;
import java.util.Base64;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.cg.curlygirl.model.ConditionerType;

@Entity
public class Conditioner {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private String name;
	private String brand;
	private BigDecimal price;
	private String placeToBuy;
	@Enumerated(EnumType.STRING)
	private ConditionerType type;
	@OneToMany(cascade=CascadeType.ALL)
	private List<Ingredient> ingredients;
	@Lob
	@Column(length=100000)
	private byte[] image;
	
	
	public Conditioner() {}

	public Conditioner(long id, String name, String brand, BigDecimal price, String placeToBuy, 
			ConditionerType type, List<Ingredient> ingredients) {
		this.id = id;
		this.name = name;
		this.brand = brand;
		this.price = price;
		this.placeToBuy = placeToBuy;
		this.type = type;
		this.ingredients = ingredients;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getPlaceToBuy() {
		return placeToBuy;
	}
	public void setPlaceToBuy(String placeToBuy) {
		this.placeToBuy = placeToBuy;
	}
	public ConditionerType getType() {
		return type;
	}
	public void setType(ConditionerType type) {
		this.type = type;
	}
	public List<Ingredient> getIngredients() {
		return ingredients;
	}
	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
}
