package com.cg.curlygirl.model.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class IngredientDTO {

	@NotBlank
	private String name;
	@NotNull
	private Boolean isCompliesWithCg;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean isCompliesWithCg() {
		return isCompliesWithCg;
	}
	public void setCompliesWithCg(Boolean isCompliesWithCg) {
		this.isCompliesWithCg = isCompliesWithCg;
	}
}
