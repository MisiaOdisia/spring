package com.cg.curlygirl.repository;

import org.springframework.data.repository.CrudRepository;

import com.cg.curlygirl.model.entity.Conditioner;

public interface ConditionerRepository extends CrudRepository<Conditioner, Long>{

}
