package com.cg.curlygirl.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Ingredient {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private String name;
	private boolean isCompliesWithCg;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isCompliesWithCg() {
		return isCompliesWithCg;
	}
	public void setCompliesWithCg(boolean isCompliesWithCg) {
		this.isCompliesWithCg = isCompliesWithCg;
	}
}
