package com.cg.curlygirl;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cg.curlygirl.controller.ConditionerController;
import com.cg.curlygirl.controller.IngredientController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CurlyGirlApplicationTests {
	
	@Autowired
	private ConditionerController conditionerController;
	@Autowired
	private IngredientController ingredientController;

	@Test
	public void contextLoads() {
		assertNotNull(conditionerController);
		assertNotNull(ingredientController);
	}
}
