package com.cg.curlygirl.controller;

import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.cg.curlygirl.model.ConditionerType;
import com.cg.curlygirl.model.dto.ConditionerDTO;
import com.cg.curlygirl.model.entity.Conditioner;
import com.cg.curlygirl.model.entity.Ingredient;
import com.cg.curlygirl.repository.ConditionerRepository;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.internal.DefaultsImpl;
import com.jayway.jsonpath.spi.json.JacksonJsonProvider;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import com.jayway.jsonpath.spi.mapper.MappingProvider;

@RunWith(SpringRunner.class)
@WebMvcTest(ConditionerController.class)
public class ConditionerControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ConditionerRepository conditionerRepository;
	@MockBean
	private ModelMapper modelMapper;

	@Before
	public void before() {
		configJsonProvider();
		when(modelMapper.map(Mockito.any(ConditionerDTO.class), Mockito.any(Class.class))).thenReturn(new Conditioner());
	}

	@After
	public void after() {
		Configuration.setDefaults(DefaultsImpl.INSTANCE);
	}

	@Test
	public void shouldGiveConditioners_whenGetAll() throws Exception {
		Ingredient[] ingredients = { new Ingredient(), new Ingredient() };
		Conditioner conditioner = new Conditioner(1, "Argan Oil", "Alterra", new BigDecimal(12.99), "Rossmann",
				ConditionerType.EMOLLIENT, Arrays.asList(ingredients));
		List<Conditioner> conditioners = Arrays.asList(conditioner);

		when(conditionerRepository.findAll()).thenReturn(conditioners);

		mockMvc.perform(
					get("/api/v1/conditioners")
					.contentType(MediaType.APPLICATION_JSON)
				)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", Matchers.hasSize(1)))
				.andExpect(jsonPath("$[0].id", is(conditioner.getId())))
				.andExpect(jsonPath("$[0].name", is(conditioner.getName())))
				.andExpect(jsonPath("$[0].brand", is(conditioner.getBrand())))
				.andExpect(jsonPath("$[0].price", is(conditioner.getPrice())))
				.andExpect(jsonPath("$[0].placeToBuy", is(conditioner.getPlaceToBuy())))
				.andExpect(jsonPath("$[0].type", is(conditioner.getType().toString())))
				.andExpect(jsonPath("$[0].ingredients", Matchers.hasSize(2)));
	}
	
	@Test
	public void shouldGiveOneConditioner_whenGet() throws Exception {
		long id = 10;
		Ingredient[] ingredients = { new Ingredient(), new Ingredient() };
		Conditioner conditioner = new Conditioner(id, "Argan Oil", "Alterra", new BigDecimal(12.99), "Rossmann",
				ConditionerType.EMOLLIENT, Arrays.asList(ingredients));
		
		when(conditionerRepository.findById(id)).thenReturn(Optional.of(conditioner));
		
		mockMvc.perform(
					get("/api/v1/conditioners/{id}", id)
					.contentType(MediaType.APPLICATION_JSON)
				)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(conditioner.getId())))
				.andExpect(jsonPath("$.name", is(conditioner.getName())))
				.andExpect(jsonPath("$.brand", is(conditioner.getBrand())))
				.andExpect(jsonPath("$.price", is(conditioner.getPrice())))
				.andExpect(jsonPath("$.placeToBuy", is(conditioner.getPlaceToBuy())))
				.andExpect(jsonPath("$.type", is(conditioner.getType().toString())))
				.andExpect(jsonPath("$.ingredients", Matchers.hasSize(2)));
	}
	
	@Test
	public void shouldGiveNotFound_whenGetWrongId() throws Exception {
		long id = 10;
		
		when(conditionerRepository.findById(id)).thenReturn(Optional.empty());
		
		mockMvc.perform(
					get("/api/v1/conditioners/{id}", id)
					.contentType(MediaType.APPLICATION_JSON)
				)
				.andExpect(status().isNotFound());
	}
	
	@Test
	public void shouldReturnId_whenSaveConditioner() throws Exception {
		Conditioner conditioner = new Conditioner();
		conditioner.setId(10);		
		
		when(conditionerRepository.save(Mockito.any(Conditioner.class))).thenReturn(conditioner);
		
		mockMvc.perform(
					post("/api/v1/conditioners")
					.contentType(MediaType.APPLICATION_JSON)
					.content("{\"name\":\"Argan Oil\",\"brand\":\"Alterra\",\"price\":12.99,\"placeToBuy\":\"Sklep Anwen\"," + 
							"\"type\": \"HUMECTANT\",\"ingredients\":[{\"name\":\"oil\",\"compliesWithCg\": false}]}")
				)
				.andExpect(status().isCreated())
				.andExpect(content().string(String.valueOf(conditioner.getId())));
	}
	
	@Test
	public void shouldGiveBadRequest_whenSaveConditionerWithoutName() throws Exception {
		when(conditionerRepository.save(Mockito.any(Conditioner.class))).thenReturn(new Conditioner());
		
		mockMvc.perform(
					post("/api/v1/conditioners")
					.contentType(MediaType.APPLICATION_JSON)
					.content("{\"name\":\"\",\"brand\":\"Alterra\",\"price\":12.99,\"placeToBuy\":\"Sklep Anwen\"," + 
							"\"type\": \"HUMECTANT\",\"ingredients\":[{\"name\":\"oil\",\"compliesWithCg\": false}]}")
				)
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$", Matchers.hasSize(1)))
				.andExpect(jsonPath("$[0].field", is("name")))
				.andExpect(jsonPath("$[0].message", is("must not be blank")));
	}
	
	@Test
	public void shouldGiveBadRequest_whenSaveConditionerWithoutBrand() throws Exception {
		when(conditionerRepository.save(Mockito.any(Conditioner.class))).thenReturn(new Conditioner());
		
		mockMvc.perform(
					post("/api/v1/conditioners")
					.contentType(MediaType.APPLICATION_JSON)
					.content("{\"name\":\"Argan Oil\",\"price\":12.99,\"placeToBuy\":\"Sklep Anwen\"," + 
							"\"type\": \"HUMECTANT\",\"ingredients\":[{\"name\":\"oil\",\"compliesWithCg\": false}]}")
				)
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$", Matchers.hasSize(1)))
				.andExpect(jsonPath("$[0].field", is("brand")))
				.andExpect(jsonPath("$[0].message", is("must not be blank")));
	}
	
	@Test
	public void shouldGiveBadRequest_whenSaveConditionerWithNegativePrice() throws Exception {
		when(conditionerRepository.save(Mockito.any(Conditioner.class))).thenReturn(new Conditioner());
		
		mockMvc.perform(
					post("/api/v1/conditioners")
					.contentType(MediaType.APPLICATION_JSON)
					.content("{\"name\":\"Argan Oil\",\"brand\":\"Alterra\",\"price\":-5.00,\"placeToBuy\":\"Sklep Anwen\"," + 
							"\"type\": \"HUMECTANT\",\"ingredients\":[{\"name\":\"oil\",\"compliesWithCg\": false}]}")
				)
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$", Matchers.hasSize(1)))
				.andExpect(jsonPath("$[0].field", is("price")))
				.andExpect(jsonPath("$[0].message", is("must be greater than or equal to 0")));
	}
	
	@Test
	public void shouldGiveBadRequest_whenSaveConditionerWithPriceOutOfBounds() throws Exception {
		when(conditionerRepository.save(Mockito.any(Conditioner.class))).thenReturn(new Conditioner());
		
		mockMvc.perform(
					post("/api/v1/conditioners")
					.contentType(MediaType.APPLICATION_JSON)
					.content("{\"name\":\"Argan Oil\",\"brand\":\"Alterra\",\"price\":5.1234567,\"placeToBuy\":\"Sklep Anwen\"," + 
							"\"type\": \"HUMECTANT\",\"ingredients\":[{\"name\":\"oil\",\"compliesWithCg\": false}]}")
				)
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$", Matchers.hasSize(1)))
				.andExpect(jsonPath("$[0].field", is("price")))
				.andExpect(jsonPath("$[0].message", is("numeric value out of bounds (<6 digits>.<2 digits> expected)")));
	}
	
	@Test
	public void shouldGiveBadRequest_whenSaveConditionerWithNegativePriceAndWithoutName() throws Exception {
		when(conditionerRepository.save(Mockito.any(Conditioner.class))).thenReturn(new Conditioner());
		
		mockMvc.perform(
					post("/api/v1/conditioners")
					.contentType(MediaType.APPLICATION_JSON)
					.content("{\"brand\":\"Alterra\",\"price\":-5.00,\"placeToBuy\":\"Sklep Anwen\"," + 
							"\"type\": \"HUMECTANT\",\"ingredients\":[{\"name\":\"oil\",\"compliesWithCg\": false}]}")
				)
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$", Matchers.hasSize(2)))
				.andExpect(jsonPath("$[0].field", is("name")))
				.andExpect(jsonPath("$[0].message", is("must not be blank")))
				.andExpect(jsonPath("$[1].field", is("price")))
				.andExpect(jsonPath("$[1].message", is("must be greater than or equal to 0")));
	}
	
	@Test
	public void shouldGiveBadRequest_whenSaveConditionerWithoutType() throws Exception {
		when(conditionerRepository.save(Mockito.any(Conditioner.class))).thenReturn(new Conditioner());
		
		mockMvc.perform(
					post("/api/v1/conditioners")
					.contentType(MediaType.APPLICATION_JSON)
					.content("{\"name\":\"Argan Oil\",\"brand\":\"Alterra\",\"price\":5.12,\"placeToBuy\":\"Sklep Anwen\"," + 
							"\"ingredients\":[{\"name\":\"oil\",\"compliesWithCg\": false}]}")
				)
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$", Matchers.hasSize(1)))
				.andExpect(jsonPath("$[0].field", is("type")))
				.andExpect(jsonPath("$[0].message", is("must not be null")));
	}
	
	@Test
	public void shouldGiveBadRequest_whenSaveConditionerWithEmptyIngredients() throws Exception {
		when(conditionerRepository.save(Mockito.any(Conditioner.class))).thenReturn(new Conditioner());
		
		mockMvc.perform(
					post("/api/v1/conditioners")
					.contentType(MediaType.APPLICATION_JSON)
					.content("{\"name\":\"Argan Oil\",\"brand\":\"Alterra\",\"price\":5.12,\"placeToBuy\":\"Sklep Anwen\"," + 
							"\"type\": \"HUMECTANT\",\"ingredients\":[]}")
				)
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$", Matchers.hasSize(1)))
				.andExpect(jsonPath("$[0].field", is("ingredients")))
				.andExpect(jsonPath("$[0].message", is("must not be empty")));
	}
	
	@Test
	public void shouldReturnUpdatedConditioner_whenUpdateConditioner() throws Exception {
		long id = 10;
		Ingredient[] ingredients = { new Ingredient(), new Ingredient() };
		Conditioner conditioner = new Conditioner(id, "Argan Oil", "Alterra", new BigDecimal(12.99), "Rossmann",
				ConditionerType.EMOLLIENT, Arrays.asList(ingredients));	
		
		when(conditionerRepository.findById(id)).thenReturn(Optional.of(conditioner));
		when(conditionerRepository.save(Mockito.any(Conditioner.class))).thenReturn(conditioner);
		
		mockMvc.perform(
					put("/api/v1/conditioners/{id}", id)
					.contentType(MediaType.APPLICATION_JSON)
					.content("{\"name\":\"Argan Oil\",\"brand\":\"Alterra\",\"price\":12.99,\"placeToBuy\":\"Sklep Anwen\"," + 
							"\"type\": \"HUMECTANT\",\"ingredients\":[{\"name\":\"oil\",\"compliesWithCg\": false}]}")
				)
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(conditioner.getId())))
				.andExpect(jsonPath("$.name", is(conditioner.getName())))
				.andExpect(jsonPath("$.brand", is(conditioner.getBrand())))
				.andExpect(jsonPath("$.price", is(conditioner.getPrice())))
				.andExpect(jsonPath("$.placeToBuy", is(conditioner.getPlaceToBuy())))
				.andExpect(jsonPath("$.type", is(conditioner.getType().toString())))
				.andExpect(jsonPath("$.ingredients", Matchers.hasSize(2)));
	}
	
	@Test
	public void shouldGiveNotFound_whenPutWithWrongId() throws Exception {
		long id = 10;
		
		when(conditionerRepository.findById(id)).thenReturn(Optional.empty());
		
		mockMvc.perform(
					get("/api/v1/conditioners/{id}", id)
					.contentType(MediaType.APPLICATION_JSON)
				)
				.andExpect(status().isNotFound());
	}
	
	@Test
	public void shoulGiveSuccess_whenDeleteConditioner() throws Exception {
		long id = 10;
		
		when(conditionerRepository.findById(id)).thenReturn(Optional.of(new Conditioner()));
		
		mockMvc.perform(delete("/api/v1/conditioners/{id}", id)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	@Test
	public void shoulGiveNotFound_whenDeleteConditionerWithWrongId() throws Exception {
		long id = 10;
		
		when(conditionerRepository.findById(id)).thenReturn(Optional.empty());
		
		mockMvc.perform(delete("/api/v1/conditioners/{id}", id)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	private void configJsonProvider() {
		final ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS);
		objectMapper.enable(DeserializationFeature.USE_LONG_FOR_INTS);

		configJsonProvider(objectMapper);
	}

	private void configJsonProvider(ObjectMapper objectMapper) {
		Configuration.setDefaults(new Configuration.Defaults() {
			private final JsonProvider jsonProvider = new JacksonJsonProvider(objectMapper);
			private final MappingProvider mappingProvider = new JacksonMappingProvider(objectMapper);

			@Override
			public JsonProvider jsonProvider() {
				return jsonProvider;
			}

			@Override
			public MappingProvider mappingProvider() {
				return mappingProvider;
			}

			@Override
			public Set<Option> options() {
				return EnumSet.noneOf(Option.class);
			}

		});
	}
}
